# Automation utils

**Reusable solutions for automation with Python**

## Use cases to be found:
* Sending emails from Python code (only Sendgrid API token required, no SMTP)
* Gitlab CI (Continuous Integration) pipelines:
    * Running unit tests of all commits and showing in GitLab
    * Measuring test coverage and showint in GitLab
* Some basics for Python development: 
    * Unit testing
    * Logging

## Ideas for next use cases:
* Schedule sending of email on GitLab (via pipeline in gitlab-ci.yml)
