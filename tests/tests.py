"""Test project."""

import unittest
import email.test


class DummyTests(unittest.TestCase):
    """Some dummy unit tests."""

    def test_upper(self):
        """Succeed always."""
        self.assertEqual(True, True)

    def test_test(self):
        """Test 'test'."""
        self.assertEqual(email.test.hello_world(), "hello_world")
