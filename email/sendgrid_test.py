"""
Use SendGrid's Python Library.

https://github.com/sendgrid/sendgrid-python
"""

import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Content, Email, Mail


def send_email(recipient=None, token=None):
    """Send an email using sendgrid."""
    if (not isinstance(recipient, str)) or (not isinstance(token, str)):
        raise Exception('Input parameters should be of type "str".')
    if (recipient is None):
        raise Exception('No recepient email given.')

    sg = SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email("jensen.thorben@gmail.com")
    to_email = Email(recipient)
    subject = "Sending with SendGrid is Fun"
    content = Content("text/plain",
                      "and easy to do anywhere, even with Python")
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())

    print(response.status_code)
    print(response.body)
    print(response.headers)


if __name__ == "__main__":
    # read secret token
    script_dir = os.path.dirname(__file__)
    with open(os.path.join(script_dir, '../sendgrid.secret'), 'r') as myfile:
        token = myfile.read()
    # send email
    send_email(recipient='jensen.thorben@gmail.com', token=token)
