"""
Simple examples for logging in python.

Source: https://docs.python.org/3/howto/logging.html#logging-basic-tutorial
"""

import logging

# configuring logging
logging.basicConfig(
    filename='example.log', level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# some different log levels
logging.debug('This message should go to the log file')
logging.info('So should this')
logging.warning('And this, too')

# logging variables
look = 'Look'
leap = 'leap!'
logging.warning('%s before you %s', look, leap)
